<?php 
$video_file = isset($_GET['file_part'])?$_GET['file_part']:false;
$thumb_file = isset($_GET['tumb'])?"thumbs/".$_GET['tumb']:false;
$is_file_exit = file_exists($video_file)?true:false;
$fount_file = ($video_file AND $is_file_exit)?true:false;
if (!$fount_file)
  die("Somethings is wrong.");
?><!DOCTYPE html>
<html>
  <head>
    <link href="https://vjs.zencdn.net/7.1.0/video-js.css" rel="stylesheet">
    <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
    <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
    <style type="text/css">
      html , body {
        margin: 0;
        padding: 0;
      }
      video, #my-video, .video-js, .vjs-default-skin
      {
        position: fixed; 
        right: 0; 
        bottom: 0;
        min-width: 100%; 
        min-height: 100%;
        width: auto; 
        height: auto; 
        z-index: -100;
      }
    </style>
  </head>
<body>
  <video 
    id="my-video"
    controls
    preload="auto"
    poster="<?= $thumb_file ?>"
    class="video-js vjs-default-skin vjs-big-play-centered vjs-16-9"
    data-setup='{"fluid": true}' 
   >
    <source src="./<?= $video_file ?>" type='video/mp4'>
    <p class="vjs-no-js">
      To view this video please enable JavaScript, and consider upgrading to a web browser that
      <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
    </p>
  </video>
  <script src="https://vjs.zencdn.net/7.1.0/video.js"></script>
</body>
</html>
